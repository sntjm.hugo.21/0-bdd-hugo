CREATE TABLE personnel(
id INT PRIMARY KEY NOT NULL,
nom TEXT,
prenom TEXT,
poste TEXT,
email TEXT NOT NULL UNIQUE
);
SELECT * FROM personnel;